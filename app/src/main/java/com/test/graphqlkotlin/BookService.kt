package com.test.graphqlkotlin

import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import kotlin.streams.asSequence

class BookService : BaseService() {

    fun fetchBooks(callBack: (List<String>) -> Unit) {
        apolloClient.query(BookListQuery())
            ?.enqueue(object : ApolloCall.Callback<BookListQuery.Data>() {
                override fun onFailure(error: ApolloException) {
                    callBack.invoke(listOf())
                }

                override fun onResponse(response: Response<BookListQuery.Data>) {
                    callBack.invoke(response.data!!.books!!.map { it!!.name })
                }
            })
    }

    fun addBook(callBack: (Boolean) -> Unit) {
        apolloClient.mutate(AddBookMutation(generateRandomString(), 7))
            ?.enqueue(object : ApolloCall.Callback<AddBookMutation.Data>() {
                override fun onFailure(error: ApolloException) {
                    callBack.invoke(false)
                }


                override fun onResponse(response: Response<AddBookMutation.Data>) {
                    callBack.invoke(true)
                }
            })
    }

    private fun generateRandomString(): String {
        val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        return java.util.Random().ints(10, 0, source.length)
            .asSequence()
            .map(source::get)
            .joinToString("")
    }

}