package com.test.graphqlkotlin

import android.os.Bundle
import android.widget.AbsListView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private lateinit var adapter: ArrayAdapter<String>
    private var _bookService = BookService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        refreshLayout.setOnRefreshListener {
            adapter.clear()
            fetchData()
        }

        addNewBook.setOnClickListener {
            adapter.clear()
            addNewData()
        }

        list.setOnScrollListener(object : AbsListView.OnScrollListener {
            override fun onScrollStateChanged(view: AbsListView, scrollState: Int) {}
            override fun onScroll(
                view: AbsListView,
                firstVisibleItem: Int,
                visibleItemCount: Int,
                totalItemCount: Int
            ) {
                refreshLayout.isEnabled = firstVisibleItem == 0
            }
        })

        fetchData()
    }

    private fun fetchData() {
        _bookService.fetchBooks {
            runOnUiThread {
                adapter =
                    ArrayAdapter(this@MainActivity, android.R.layout.simple_list_item_1, it)
                list.adapter = adapter
                refreshLayout.isRefreshing = false
            }
        }
    }

    private fun addNewData() {
        _bookService.addBook { if (it) fetchData() }
    }
}