package com.test.graphqlkotlin

import com.apollographql.apollo.ApolloClient

open class BaseService {
    val apolloClient: ApolloClient = ApolloClient.builder()
        .serverUrl("http://178.157.14.21:5537/graphql")
        .build()
}